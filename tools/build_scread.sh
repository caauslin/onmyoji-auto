#!/usr/bin/env bash

set -e

cd android/scread
rm -r libs obj
ndk-build

adb shell rm /data/local/tmp/scread
adb push libs/arm64-v8a/scread /data/local/tmp
