#!/usr/bin/env bash

set -e

find . -name "*.pyc" -type f -delete
