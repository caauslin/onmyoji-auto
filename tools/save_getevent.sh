#!/usr/bin/env bash

set -e

function getevent {
    name="$1"
    if [[ -z "$name" ]]; then
        name="$(date)"
    fi
    adb shell getevent -t "$2" > "$name.touches"
}

getevent "$@"
