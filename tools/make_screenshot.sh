#!/usr/bin/env bash

set -e

function screencap {
    name="$1"
    if [[ -z "$name" ]]; then
        name="$(date)"
    fi
    adb shell "screencap -p '/sdcard/$name.png'"
    adb pull "/sdcard/$name.png" .
    adb shell "rm '/sdcard/$name.png'"
}

screencap "$@"
