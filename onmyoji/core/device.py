# -*- coding: utf-8 -*-

import re
from . import adb

class AdbCommandError(Exception):
    pass

class DeviceProxy(object):
    """Manages android devices via adb."""

    def __init__(self, serial_no):
        self._serial_no = serial_no
        try:
            if self.send_command('get-state') != 'device':
                self.send_command('wait-for-device', timeout=10)
        except:
            raise

    def _raise_exception(self, message):
        raise AdbCommandError('{0}: {1}'.format(self._serial_no, message))

    def send_command(self, *args, **kwargs):
        """Wrapper function for adb.send_command with timeout support."""
        kwargs['device'] = self._serial_no
        return adb.send_command(*args, **kwargs)

    # ===== Process Management =====

    def start_package(self, package):
        """Starts the main activity of specified package."""
        self.send_command('shell', 'monkey', '-p', package,
                          '-c', 'android.intent.category.LAUNCHER', '1')

    def force_stop(self, package):
        """Force stops all activities from specified package."""
        self.send_command('shell', 'am', 'force-stop', package)

    def is_process_running(self, package):
        """Checks if the main process (not background service) of specified
        package is running, either in foreground or background.
        """
        output = self.send_command('shell', 'ps', package)
        for line in output.splitlines():
            match = re.search(r'.*{0}$'.format(package), line)
            if match:
                return True
        return False

    # ===== Data Operations =====

    def clear_package(self, package):
        """Erases all the data associated with specified package, same as CLEAR
        DATA option in Apps settings.
        """
        self.send_command('shell', 'pm', 'clear', package)

    def move(self, source, target):
        """Moves file/directory."""
        self.send_command('shell', 'mv', source, target)

    def delete(self, *files):
        """Deletes files/directories"""
        self.send_command('shell',
                          'rm -r ' + ' '.join('"{0}"'.format(f) for f in files),
                          check_shell=False)

    def push(self, source, target):
        """Copys local file/directory to device."""
        self.send_command('push', source, target)

    # ===== Touch Screen =====

    def get_screen_size(self):
        output = self.send_command('shell', 'wm size')
        match = re.search(r'(\d+)x(\d+)$', output.strip())
        if match is None:
            self._raise_exception('cannot fetch screen size')
        return (int(match.group(1)), int(match.group(2)))

    def get_orientation(self):
        """Returns the current screen orientation.

        Returns:
            0 for unchanged
            1 for  90 anticlockwise rotation
            2 for 180 anticlockwise rotation
            3 for 270 anticlockwise rotation
        """
        output = self.send_command('shell',
                                   'dumpsys input | grep SurfaceOrientation')
        match = re.match(r'\D*(\d)', output)
        if not match:
            self._raise_exception('cannot fetch device orientation')
        else:
            return int(match.group(1))

    def get_screen_pixel(self, x, y):
        """Returns the RGB color of specified pixel on screen."""
        if x < 0 or y < 0:
            self._raise_exception('cannot read off-screen pixel {0},{1}'.format(x, y))

        self.send_command('shell', 'screencap /sdcard/screenshot.raw')
        header_data = self.send_command(
            'shell', 'dd if=/sdcard/screenshot.raw bs=12 count=1 2> /dev/null'
        ).encode('hex')

        if len(header_data) != 24:
            self._raise_exception('cannot read screen pixel, bad image header')

        header_data_blocks = [header_data[i:i + 2] for i in range(0, len(header_data), 2)]
        screen_width = int(''.join(header_data_blocks[3::-1]), 16)
        screen_height = int(''.join(header_data_blocks[7:3:-1]), 16)
        if x >= screen_width or y >= screen_height:
            self._raise_exception('cannot read off-screen pixel {0},{1}'.format(x, y))

        blocks_skip = 3 + screen_width * y + x
        pixel_rgba = self.send_command(
            'shell',
            'dd if=/sdcard/screenshot.raw bs=4 count=1 '
            'skip={0} 2> /dev/null'.format(blocks_skip)
        ).encode('hex')
        return pixel_rgba[:6]

    def get_screen_pixels(self, *args):
        """Returns the RGB colors of specified pixels on screen. This function
        requires `scread` to be installed at `/data/local/tmp/` on the device.
        """
        pixels_arg = ''
        for i in range(0, len(args), 2):
            pixels_arg += '{0},{1} '.format(args[i], args[i+1])
        output = self.send_command(
            'shell',
            'screencap | /data/local/tmp/scread {0}'.format(pixels_arg[:-1])
        ).encode('hex')
        colors = []
        for rgb in [output[i:i+6] for i in range(0, len(output), 8)]:
            colors.append(rgb)
        return colors;

    # ===== Utilities =====

    def get_model(self):
        """Return the model name of the device."""
        model = self.send_command('shell', 'getprop ro.product.model')
        return model.strip().lower().replace(' ', '_')

if __name__ == '__main__':
    # python -i -m onmyoji.core.device
    dev = DeviceProxy(adb.get_devices()[0])
