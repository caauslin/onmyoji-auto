# -*- coding: utf-8 -*-

import logging
import logging.config

class ConsoleFormatter(logging.Formatter):
    """Format the console logging messages."""

    def __init__(self):
        logging.Formatter.__init__(self, '[%(name)s:%(levelname)s] %(message)s')

    LOGLEVEL_COLORS = {
        logging.DEBUG: '\033[2m',
        logging.INFO: '\033[34m',
        logging.WARNING: '\033[33m',
        logging.ERROR: '\033[31m'
    }
    LOGLEVEL_LABELS = {
        logging.DEBUG: 'DBUG',
        logging.INFO: 'INFO',
        logging.WARNING: 'WARN',
        logging.ERROR: 'ERRO'
    }

    def format(self, record):
        if record.levelno in self.LOGLEVEL_LABELS:
            record.levelname = self.LOGLEVEL_LABELS[record.levelno]
        if record.levelno in self.LOGLEVEL_COLORS:
            record.levelname = '{0}{1}{2}'.format(
                self.LOGLEVEL_COLORS[record.levelno], record.levelname, '\033[0m'
            )
        return logging.Formatter.format(self, record)

# config logging module
logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'console': {'()': ConsoleFormatter}
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'console',
            'stream': 'ext://sys.stdout'
        }
    },
    'loggers': {
        '': {'handlers': ['console'], 'level': 'DEBUG'}
    }
})

def get_logger(channel=''):
    return logging.getLogger(channel)

# def log(severity, message, *args, **kwargs):
#     """Log a message using default logger."""
#     get_logger().log(severity, message, *args, **kwargs)
#
# def debug(message, *args, **kwargs):
#     log(logging.DEBUG, message, *args, **kwargs)
#
# def info(message, *args, **kwargs):
#     log(logging.INFO, message, *args, **kwargs)
#
# def warning(message, *args, **kwargs):
#     log(logging.WARNING, message, *args, **kwargs)
#
# def error(message, *args, **kwargs):
#     log(logging.ERROR, message, *args, **kwargs)
#
# def exception(msg, *args, **kwargs):
#     kwargs['exc_info'] = True
#     error(msg, *args, **kwargs)
