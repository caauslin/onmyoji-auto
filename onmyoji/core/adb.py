# -*- coding: utf-8 -*-
"""Helper functions for executing adb commands."""
# TODO add timeout support for `subprocess` module on python 2
# from multiprocessing import Process
# proc = Process(target=f, args=args, kwargs=kwargs)

import re
import subprocess
import sys

# the name/path of the adb executable
_ADB_PROGRAM = 'adb'

def _quote_argument(arg):
    """Return a shell-safe argument string with spaces escaped."""
    # TODO confirm if works on windows (windows itself uses ^ for escaping)
    if re.search(r'([^\\]|^) ', arg):
        return '"' + arg + '"'
    else:
        return arg

def restart():
    """Restart the adb daemon."""
    subprocess.check_call([_ADB_PROGRAM, 'kill-server'])
    subprocess.check_call([_ADB_PROGRAM, 'start-server'])

def get_devices():
    """Return a list of connected devices."""
    output = subprocess.check_output([_ADB_PROGRAM, 'devices'])
    devices = []
    for line in output.splitlines():
        match = re.match(r'([0-9a-z\.:-]+)\s+device$', line.strip())
        if not match:
            continue
        devices.append(match.group(1))
    return devices

def send_command(*args, **kwargs):
    """
    Execute adb command on specified device, and return the output from stdout
    unless `print_stdout` is set to `True`.

    Example:
        send_command('shell', 'ls', print_stdout=True)
    """
    if len(args) == 0:
        raise ValueError('adb command cannot run without argument')
    args = list(args)

    device = kwargs.get('device')
    check_shell = kwargs.get('check_shell', True)
    print_stdout = kwargs.get('print_stdout', False)

    # `adb` is not needed here
    if args[0] == 'adb':
        args.pop(0)

    # build command
    command = [_ADB_PROGRAM]
    if device is not None:
        command.extend(['-s', device])

    is_shell_command = args[0] == 'shell'
    # NOTE shell command should be sent in one single string, or it might be
    # interpreted incorrectly if contains |, && or ||
    if is_shell_command:
        args.pop(0)
        if len(args) == 0:
            raise ValueError('cannot start adb interactive shell')

        command.append('shell')
        if len(args) == 1:
            shell_body = args[0]
        else:
            shell_body = ' '.join([_quote_argument(shb) for shb in args])

        if check_shell:
            # NOTE print the exit code at the end of stdout
            shell_body += ' && printf "#$?" || printf "#$?"'
        command.append(shell_body)
    else:
        command.extend(args)

    output = subprocess.check_output(command)
    if is_shell_command and check_shell:
        # check the exit state
        output, exit_code = output.rsplit('#', 1)
        if exit_code != '0':
            sys.stdout.write(output)
            sys.stdout.flush()
            raise subprocess.CalledProcessError(exit_code, command, output=output)

    if print_stdout:
        sys.stdout.write(output)
        sys.stdout.flush()
    return output
