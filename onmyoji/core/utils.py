# -*- coding: utf-8 -*-

import math
import random

def colors_distance(target, source, squred=False):
    """Calculate the euclidean distance of two RGB colors in hex format."""
    if target.lower() == source.lower():
        return 0

    rgb_a = (int(target[:2], 16), int(target[2:4], 16), int(target[4:6], 16))
    rgb_b = (int(source[:2], 16), int(source[2:4], 16), int(source[4:6], 16))

    return eucl_distance(rgb_a, rgb_b, squred)

def eucl_distance(begin, end, squred=False):
    """Calculate the euclidean distance of two same-dimension vectors."""
    diff = [begin[i] - end[i] for i in range(0, len(begin))]
    distance_square = sum([n * n for n in diff])
    if squred:
        return distance_square
    else:
        return math.sqrt(distance_square)

def random_within_circle(x, y, radius):
    """Return a random position with a circle."""
    # http://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
    t = 2.0 * math.pi * random.uniform(0, 1)
    u = random.uniform(0, radius) + random.uniform(0, radius)
    r = 2.0 * radius - u if u > radius else u
    return (int(r * math.cos(t)) + x, int(r * math.sin(t)) + y)
