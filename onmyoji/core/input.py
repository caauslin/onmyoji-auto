# -*- coding: utf-8 -*-

import collections
import operator
import random
import re
import time
from . import log
from .utils import eucl_distance

EV_SYN = int('0x00', 16)
EV_KEY = int('0x01', 16)
EV_ABS = int('0x03', 16)

SYN_REPORT = int('0x00', 16)

ABS_MT_TRACKING_ID = int('0x39', 16)
ABS_MT_SLOT = int('0x2f', 16) #47
ABS_MT_POSITION_X = int('0x35', 16) #53
ABS_MT_POSITION_Y = int('0x36', 16) #54
ABS_MT_PRESSURE = int('0x3a', 16) #58
ABS_MT_TOUCH_MAJOR = int('0x30', 16)
ABS_MT_TOUCH_MINOR = int('0x31', 16)

class InputProxy(object):
    """Simulate input events via ADB."""

    def __init__(self, device, touchscreen=None):
        self._device = device
        self._touchscreen = touchscreen
        self._screen_width, self._screen_height = device.get_screen_size()
        self._screen_orientation = 0
        self._taps = []
        self._logger = log.get_logger('input')

    def load_touch_profile(self, filepath):
        reader = TouchEventsReader()
        reader.read_file(filepath)
        for touch_data in reader.data:
            touch = touch_data.bake()
            if touch.distance <= 2 and touch.duration <= 0.1:
                self._taps.append(touch)
        self._logger.debug('total %d taps loaded', len(self._taps))

    def refresh_orientation(self):
        self._screen_orientation = self._device.get_orientation()

    def tap(self, x, y):
        if len(self._taps) == 0 or self._touchscreen is None:
            self._logger.debug('tapping (simple) ({0},{1})'.format(x, y))
            self._device.send_command('shell', 'input tap {0} {1}'.format(x, y))
            return

        if self._screen_orientation == 1:
            x, y = self._screen_width - 1 - y, x
        else:
            raise NotImplementedError('screen orientation {0} is not supported'.format(
                self._screen_orientation))

        prototype = self._taps[random.randint(0, len(self._taps) - 1)]
        current_x, current_y = x, y
        command = ''
        for i in range(0, len(prototype.data) - 1, 2):
            if i != 0:
                command += 'sleep {0} && '.format(prototype.data[i])

            if prototype.data[i+1] is None:
                command += 'sendevent {0} {1} {2} 0 && '.format(
                    self._touchscreen, EV_ABS, ABS_MT_PRESSURE)
                command += 'sendevent {0} {1} {2} 0'.format(
                    self._touchscreen, EV_SYN, SYN_REPORT)
            else:
                for evt_code in prototype.data[i+1]:
                    if evt_code == ABS_MT_POSITION_X:
                        current_x += prototype.data[i+1][evt_code] - prototype.begin[0]
                        command += 'sendevent {0} {1} {2} {3} && '.format(
                            self._touchscreen, EV_ABS, evt_code, current_x)
                    elif evt_code == ABS_MT_POSITION_Y:
                        command += 'sendevent {0} {1} {2} {3} && '.format(
                            self._touchscreen, EV_ABS, evt_code, current_y)
                    else:
                        command += 'sendevent {0} {1} {2} {3} && '.format(
                            self._touchscreen, EV_ABS, evt_code,
                            prototype.data[i+1][evt_code])
                command += 'sendevent {0} {1} {2} 0 && '.format(
                    self._touchscreen, EV_SYN, SYN_REPORT)

        self._logger.debug(command)
        self._device.send_command('shell', command)

    def swipe(self):
        raise NotImplementedError('action swipe is not implemented')

    def text(self, text):
        for ch in text:
            self._device.send_command('shell', 'input text "{0}"'.format(ch))
            time.sleep(random.uniform(0, 0.1))

class TouchDataError(Exception):
    """Exception produced during the touch data processing."""
    pass

TouchInfo = collections.namedtuple(
    'TouchInfo', ['begin', 'end', 'duration', 'distance', 'data']
)

class TouchEventsData(object):
    """A series of input events fits a single touch, either tap or swipe."""

    def __init__(self, tracking_id):
        self._tracking_id = tracking_id
        self._is_finalized = False
        self._updates = {}
        self._is_corrupt = False
        self._data = []
        self._last_timestamp = -1

    @property
    def tracking_id(self):
        return self._tracking_id.lstrip('0')

    @property
    def is_corrupt(self):
        return self._is_corrupt

    def _raise(self, message):
        """Raise an exception."""
        raise TouchDataError('*{0}*: {1}'.format(self.tracking_id, message))

    def _verify_timestamp(self, timestamp, test=operator.ge):
        """Verify/Update the timestamp, this makes sure all touch events are
        recived in chronological order.
        """
        if self._last_timestamp == -1 or test(timestamp, self._last_timestamp):
            self._last_timestamp = timestamp
        else:
            self._is_corrupt = True
            self._raise('bad timestamp: {0}:{1}'.format(timestamp, self._last_timestamp))

    def _rasie_if_corrupt(self):
        """Raise exception when data is corrupt."""
        if self._is_corrupt:
            self._raise('data is corrupt')

    def _rasie_if_finalized(self):
        """Raise exception when data is corrupt."""
        if self._is_corrupt:
            self._raise('data is corrupt')

    def update(self, timestamp, evt_code, evt_data):
        """
        Add a new touch event. The event code is not filtered, so as to support
        unknown event type, which allows illegal data to be pushed in, be
        careful and always validate the event code before use it.
        """
        self._rasie_if_corrupt()
        self._rasie_if_finalized()

        if len(self._updates) == 0:
            self._verify_timestamp(timestamp, operator.ge)
        else:
            self._verify_timestamp(timestamp, operator.eq)

        if evt_code in self._updates:
            self._raise('duplicate touch event: {0}'.format(hex(evt_code)))
        self._updates[evt_code] = int(evt_data, 16)

    def report(self, timestamp):
        """Report pending updates to events list."""
        self._rasie_if_corrupt()
        self._rasie_if_finalized()

        if len(self._updates) == 0:
            return
        self._verify_timestamp(timestamp, operator.eq)

        # NOTE sometimes pressure data wasn't recorded during the init touch
        # contact for unknown reason, which renders re-simulation on this
        # touch impossible, better ignore it for now
        if len(self._data) == 0:
            if self._updates.get(ABS_MT_PRESSURE, None) is None:
                self._is_corrupt = True
                self._raise('no pressure data recorded in init contact')
            elif self._updates.get(ABS_MT_POSITION_X, None) is None:
                self._is_corrupt = True
                self._raise('no X coordinate data recorded in init contact')
            elif self._updates.get(ABS_MT_POSITION_Y, None) is None:
                self._is_corrupt = True
                self._raise('no Y coordinate data recorded in init contact')

        self._data.append({'timestamp': timestamp, 'events': self._updates})
        self._updates = {}

    def finalize(self, timestamp):
        """Finalize this touch."""
        self._rasie_if_corrupt()
        self._verify_timestamp(timestamp, operator.gt)

        if len(self._updates) > 0:
            self._is_corrupt = True
            self._raise('cannot finalize due to unreported updates')
        elif len(self._data) == 0:
            self._is_corrupt = True
            self._raise('cannot finalize due to missing data')

        self._data.append({'timestamp': timestamp, 'events': None})
        self._is_finalized = True

    def bake(self):
        """Prepare this touch data for InputProxy."""
        if not self._is_finalized:
            return None
        start_position = (self._data[0]['events'][ABS_MT_POSITION_X],
                          self._data[0]['events'][ABS_MT_POSITION_Y])
        distance = 0
        duration = self._data[-1]['timestamp'] - self._data[0]['timestamp']
        last_position = start_position
        last_timestamp = self._data[0]['timestamp']
        events = []

        for touch in self._data:
            events.append(touch['timestamp'] - last_timestamp)
            last_timestamp = touch['timestamp']
            if touch['events'] is None:
                events.append(None)
            else:
                events.append(dict(touch['events']))
                current_position = (
                    touch['events'].get(ABS_MT_POSITION_X, last_position[0]),
                    touch['events'].get(ABS_MT_POSITION_Y, last_position[1])
                )
                distance += eucl_distance(last_position, current_position)
                last_position = current_position

        return TouchInfo(begin=start_position, end=last_position,
            duration=duration, distance=distance, data=events)

class MultiTouchStore(object):
    """Helper class for storing multi-touch events in `TouchEventsReader`."""

    def __init__(self):
        self._elements = []
        self._slot = 0

    @property
    def slot(self):
        return self._slot

    @slot.setter
    def slot(self, value):
        self._slot = int(value, 16)

    def push(self, elem):
        for i in range(0, len(self._elements)):
            if self._elements[i] is None:
                self._elements[i] = elem
                return
        self._elements.append(elem)

    def pull(self):
        try:
            return self._elements[self._slot]
        except IndexError:
            return None

    def pop(self):
        try:
            elem = self._elements[self._slot]
            self._elements[self._slot] = None
            if self._slot == len(self._elements) - 1:
                for i in range(self._slot, -1, -1):
                    if self._elements[i] is None:
                        self._elements.pop(i)
            return elem
        except IndexError:
            return None

    def items(self):
        for elem in self._elements:
            if elem is not None:
                yield elem

class TouchEventsReader(object):
    """Process touch events generated by `getevent -t`."""

    def __init__(self):
        self._touches = MultiTouchStore()
        self._filename = None
        self._lineno = 0

        self._ignored_event_types = []
        self._ignored_syn_codes = []

        self.data = []
        self._logger = log.get_logger('read_touch')

    @staticmethod
    def _parse_raw(text):
        """Parse text to (timestamp, evt_type, evt_code, evt_data) format."""
        match = re.match(
            r'\[\s*(\d+\.\d+)\]\s+'
            r'\/[\w\/]+:\s+'
            r'([0-9a-f]+)\s+'
            r'([0-9a-f]+)\s+'
            r'([0-9a-f]+)$', text.strip())
        if not match:
            return None
        return match.groups()

    def _debug(self, message, critical=False):
        if critical:
            self._logger.error('%s:%d %s', self._filename, self._lineno, message)
        else:
            self._logger.debug('%s:%d %s', self._filename, self._lineno, message)

    def read_file(self, filepath):
        """Read events from specified file."""
        with open(filepath, 'r') as infile:
            self._filename = infile.name
            self._lineno = 0
            for line in infile:
                self.read_line(line)

    def read_line(self, line):
        """Read a single event from specified text."""
        if self._filename is None:
            self._filename = '*'
            self._lineno = 0
        self._lineno += 1

        event_data = self._parse_raw(line)
        if event_data is None:
            self._debug('invalid event data')
        else:
            timestamp, evt_type, evt_code, evt_data = event_data
            self._read_event(float(timestamp), int(evt_type, 16), int(evt_code, 16), evt_data)

    def _read_event(self, timestamp, evt_type, evt_code, evt_data):
        """
        Categorize event by its type and redirect it to corresponding parser
        function.
        """
        if evt_type == EV_SYN:
            self._read_event_syn(timestamp, evt_code, evt_data)
        elif evt_type == EV_ABS:
            self._read_event_abs(timestamp, evt_code, evt_data)
        elif evt_type not in self._ignored_event_types:
            self._ignored_event_types.append(evt_type)
            self._debug('unknown event type: {0}'.format(evt_type))

    def _read_event_syn(self, timestamp, evt_code, evt_data):
        """Process EV_SYN events."""
        if evt_code == SYN_REPORT:
            if evt_data == '00000000':
                self._report_touches(timestamp)
            else:
                pass
        elif evt_code not in self._ignored_syn_codes:
            self._ignored_syn_codes.append(evt_code)
            self._debug('unknown EV_SYN event, code: {0}'.format(evt_code))

    def _read_event_abs(self, timestamp, evt_code, evt_data):
        """Process EV_ABS events (touch events)."""
        if evt_code == ABS_MT_TRACKING_ID:
            if evt_data == 'ffffffff':
                self._finalize_touch(timestamp)
            else:
                self._init_touch(evt_data)
        elif evt_code == ABS_MT_SLOT:
            self._update_slot(evt_data)
        else:
            self._update_touch(timestamp, evt_code, evt_data)

    def _init_touch(self, tracking_id):
        """Start a new touch."""
        touch = TouchEventsData(tracking_id)
        self._touches.push(touch)

    def _update_slot(self, evt_data):
        self._touches.slot = evt_data

    def _update_touch(self, timestamp, evt_code, evt_data):
        touch = self._touches.pull()
        if touch is None:
            self._debug('update failed: touch slot {0} is empty'.format(self._touches.slot))
        elif not touch.is_corrupt:
            try:
                if not touch.is_corrupt:
                    touch.update(timestamp, evt_code, evt_data)
            except TouchDataError as err:
                self._debug('update failed: ' + err.message)

    def _report_touches(self, timestamp):
        """report all available touches, but ignore those in invalid state."""
        for touch in self._touches.items():
            if touch.is_corrupt:
                continue
            try:
                touch.report(timestamp)
            except TouchDataError as err:
                self._debug('report failed: ' + err.message)

    def _finalize_touch(self, timestamp):
        touch = self._touches.pop()
        if touch is None:
            self._debug('finalize failed: touch slot {0} is empty'.format(self._touches.slot))
        elif not touch.is_corrupt:
            try:
                touch.finalize(timestamp)
                self.data.append(touch)
            except TouchDataError as err:
                self._debug('update failed: ' + err.message)

def main():
    """Process touch events from live input."""
    import subprocess
    reader = TouchEventsReader()
    logger = log.get_logger('test')
    proc = subprocess.Popen(['adb', 'shell', 'getevent -t'], stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if line == '':
            continue
        reader.read_line(line)
        if len(reader.data) > 0:
            logger.info('>>> ' + reader.data[0].tracking_id)
            logger.info(reader.data.pop()._events)

if __name__ == '__main__':
    main()
