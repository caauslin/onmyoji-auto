# -*- coding: utf-8 -*-

from .gameplay import GameplayTask

class LoginAsGuestTask(GameplayTask):

    def run(self):
        self.logger.info('closing game announcement ...')
        self.hotspot('announcement_close_btn')
        self.wait(0.8)

        self.logger.info('start logging in ...')
        self.hotspot('quick_start_btn')
        self.wait(1)

        self.hotspot('login_as_guest_btn')
        self.wait(3.8)

        # self.logger.info('switching realm ...')
        # self.hotspot('change_realm_btn')
        # self.wait(4.2)
        #
        # self.hotspot('realm_row2_col2')
        # self.wait(1.8)

        self.logger.info('entering game ...')
        self.hotspot('enter_game_btn')
        self.wait(3.2)

        if not self.checkpoint('tos_accept_btn'):
            self.beep()
            self.prompt_input('if sees ToS, type Y to continue: ', 'y')

        self.logger.info('accepting ToS ...')
        self.hotspot('tos_accept_btn')
        self.random_wait(1.2, 1.5)

        while True:
            self.hotspot('tap_to_input_name')
            self.random_wait(1, 1.2)
            self.hotspot('name_input_area')
            self.beep()
            name = self.prompt_input('input your name, leave empty to skip: ')
            if len(name) == 0:
                break
            self.input.text(name)
            self.hotspot('keyboard_ok')
            self.wait(0.8)

            self.hotspot('name_input_confirm_btn')
            self.random_wait(0.8, 1.2)
            self.hotspot('create_char_btn')
            self.random_wait(1.8, 2.2)
            if not self.checkpoint('create_char_btn'):
                break
            else:
                self.beep()
                self.logger.warning('invalid name!')

        self.logger.info('character is ready, entering game ...')
        self.hotspot('skip_openning')
        self.random_wait(0.8, 1)
        self.hotspot('skip_openning')
        self.random_wait(8.5, 9)

        if not self.checkpoint('first_entering_game', timeout=5, retry=2.5):
            self.beep()
            self.prompt_input('if entered game (『小白』 is talking), enter Y: ', 'y')
        self.random_wait(0.8, 1)
