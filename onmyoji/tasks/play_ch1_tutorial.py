# -*- coding: utf-8 -*-

from .gameplay import GameplayTask

class PlayCharpterOneTutorialTask(GameplayTask):

    def run(self):
        self.logger.info('skipping conversations ...')
        repeat = 3
        while repeat > 0:
            self.hotspot('dialog_next')
            self.random_wait(0.6, 0.8)
            repeat -= 1
        self.random_wait(0.8, 1.2)

        # introduce 『小白』
        self.hotspot('dialog_next')
        self.random_wait(0.8, 1)

        repeat = 6
        while repeat > 0:
            self.hotspot('dialog_next')
            self.random_wait(0.6, 0.8)
            repeat -= 1
        self.random_wait(0.8, 1.2)

        # introduce 『神乐』
        self.hotspot('dialog_next')
        self.random_wait(0.8, 1)

        repeat = 5
        while repeat > 0:
            self.hotspot('dialog_next')
            self.random_wait(0.6, 0.8)
            repeat -= 1
        self.random_wait(0.8, 1.2)

        # talk to 『犬神』
        self.hotspot('ch1_scene1_inugami_talk1')
        self.random_wait(0.6, 0.8)

        self.hotspot('dialog_skip_btn')
        self.logger.info('entering battle ...')
        self.random_wait(12.5, 13.5)

        self.logger.info('skipping battle tips #1 ...')
        repeat = 3
        while repeat > 0:
            self.hotspot('skip_midscreen_tip')
            self.random_wait(0.8, 1)
            repeat -= 1

        # first attack
        self.logger.info('attack #1, waiting for enemy\'s turn ...')
        self.hotspot('arena1_mid_enemy')
        self.random_wait(7.5, 8)

        self.logger.info('skipping battle tips #2 ...')
        repeat = 2
        while repeat > 0:
            self.hotspot('skip_midscreen_tip')
            self.random_wait(0.8, 1)
            repeat -= 1

        # second attack
        self.logger.info('attack #2, waiting for enemy\'s turn ...')
        self.hotspot('arena1_mid_enemy')
        self.random_wait(5.5, 6)

        self.logger.info('skipping battle tips #3 ...')
        self.hotspot('arena1_mid_enemy')
        self.random_wait(0.8, 1)

        # third attack
        self.logger.info('attack #3, winning ...')
        self.hotspot('arena1_mid_enemy')
        self.random_wait(5.5, 6)

        # battle summary
        self.logger.info('skipping battle summary ...')
        self.hotspot('arena1_mid_enemy')
        self.random_wait(0.8, 1)

        self.logger.info('looting now ...')
        self.hotspot('skip_midscreen_tip')
        self.random_wait(2.5, 3)

        self.logger.info('going back to story mode ...')
        self.hotspot('skip_midscreen_tip')
        self.random_wait(10.5, 12.5)

        # scene 2
        self.logger.info('skipping story mode ...')
        self.logger.info('talking to 『小白』')
        self.hotspot('ch1_scene2_howaito_talk1')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')
        self.random_wait(2.5, 2.75)

        self.logger.info('fast-forwarding cutscene ...')
        self.hotspot('cutscene_fastforward_btn')
        self.random_wait(12.5, 13.5)

        self.logger.info('talking to 『神乐』')
        self.hotspot('ch1_scene2_kagura_talk1')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')
        self.random_wait(0.8, 1)

        self.logger.info('talking to 『犬神』')
        self.hotspot('ch1_scene2_inugami_talk1')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')
        self.random_wait(0.8, 1)

        self.logger.info('talking to 『小白』')
        self.hotspot('ch1_scene2_howaito_talk2')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')
        self.random_wait(0.8, 1)

        self.logger.info('talking to 『犬神』')
        self.hotspot('ch1_scene2_inugami_talk2')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')
        self.random_wait(0.8, 1)

        self.logger.info('talking to 『神乐』')
        self.hotspot('ch1_scene2_kagura_talk2')
        self.random_wait(1.6, 1.8)
        self.hotspot('dialog_skip_btn')

        self.logger.info('going back to home ...')
        self.random_wait(9.5, 10.5)

        self.logger.info('skipping in-game tutorial ...')
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(1.5, 1.8)

        self.logger.info('entering summoning room ...')
        self.hotspot('summoning_room_entry')
        self.random_wait(3.5, 4.2)

        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)

        self.logger.info('summoning the first ...')
        self.hotspot('summoning_room_btn2')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)

        self.beep()
        self.finish_summoning()

        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)

        self.logger.info('summoning the second ...')
        self.hotspot('summoning_room_btn3')
        self.random_wait(0.8, 1)
        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)

        self.beep()
        self.finish_summoning()

        self.hotspot('skip_midscreen_tip')
        self.random_wait(0.8, 1)

        self.logger.info('openning mailbox ...')
        self.hotspot('mailbox_entry')
        self.random_wait(2, 2.4)

        self.logger.info('loot mail #3 ...')
        self.loot_third_mail()

        self.logger.info('loot mail #4 ...')
        self.loot_third_mail()

        self.logger.info('back to summoning room ...')
        self.hotspot('mailbox_close_btn')
        self.random_wait(1, 1.2)

        self.beep()
        self.beep()

    def finish_summoning(self):
        count = 0
        self.beep()
        while not self.checkpoint('confirm_summoning'):
            if count >= 2:
                self.beep()
                self.prompt_input('if finished summoning, type Y to continue ', 'y')
                break
            self.wait(12)
            count += 1
        self.hotspot('confirm_summoning_btn')
        self.random_wait(0.6, 0.8)

    def loot_third_mail(self):
        self.hotspot('mailbox_mail3')
        self.random_wait(1.8, 2.2)
        self.hotspot('mailbox_loot_btn')
        self.random_wait(1, 1.2)
        self.hotspot('mailbox_clost_loot')
        self.random_wait(1, 1.2)
