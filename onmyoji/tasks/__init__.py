# -*- coding: utf-8 -*-
"""Run and manage the tasks."""

import shlex

try:
    import ConfigParser as configparser
except ImportError:
    import configparser

from ..core import log
from ..core.device import DeviceProxy
from ..core.input import InputProxy
from .base import BaseTask

class TaskLookupError(Exception):
    pass

class TaskFailedError(Exception):
    pass

class TaskManager(object):
    """Run ans manage game tasks."""

    def __init__(self, serial_no):
        self._device = DeviceProxy(serial_no)
        self._logger = log.get_logger('*')
        # load config data associated with the device
        device_model = self._device.get_model()
        self._config = configparser.ConfigParser()
        loaded = self._config.read('data/devices/{0}.ini'.format(device_model))
        if len(loaded) == 0:
            raise IOError('no device config found: {0}'.format(device_model))
        # setup input proxy
        self._input = InputProxy(self._device, self.get_config('screen.device'))
        self._input.load_touch_profile('data/devices/{0}.touches'.format(device_model))

    def get_device(self):
        return self._device

    def get_logger(self):
        return self._logger

    def get_input(self):
        return self._input

    def start_task(self, task_name, *args, **kwargs):
        import importlib
        import inspect

        # verify the name before loading
        if len(task_name) == 0 or '.' in task_name:
            raise TaskLookupError('cannot find task: {0}'.format(task_name))

        module_name = 'onmyoji.tasks.' + task_name
        task_module = importlib.import_module(module_name)
        task_classes = [
            n[1] for n in inspect.getmembers(task_module, inspect.isclass)
            if n[1].__module__ == module_name and
            issubclass(n[1], BaseTask) and
            n[1] != BaseTask and
            'run' in [m[0] for m in inspect.getmembers(n[1], inspect.ismethod)]
        ]

        if len(task_classes) != 1:
            raise TaskLookupError('cannot find task: {0}'.format(task_name))

        task = task_classes[0](self, *args, **kwargs)
        try:
            self.get_input().refresh_orientation()
            task.run()
        except TaskFailedError:
            raise
        finally:
            task.close()

    def get_raw_config(self, key):
        section, key = key.rsplit('.', 1)
        return self._config.get(section, key)

    def get_config(self, key):
        value = self.get_raw_config(key)
        value_list = shlex.split(value.strip())
        if len(value_list) == 0:
            return None
        elif len(value_list) == 1:
            return self.real_type(value)
        else:
            for i in range(0, len(value_list)):
                value_list[i] = self.real_type(value_list[i])
            return tuple(value_list)

    @staticmethod
    def real_type(raw_string):
        """Convert the raw config string to its real type."""
        try:
            return int(raw_string)
        except ValueError:
            pass
        try:
            return float(raw_string)
        except ValueError:
            pass
        return raw_string

def main():
    from ..core import adb
    logger = log.get_logger('test')
    logger.info('init device ...')
    user_input = ''
    try:
        dev = adb.get_devices()[0]
    except IndexError:
        logger.error('no device found')
        return
    tm = TaskManager(dev)
    while True:
        try:
            user_input = raw_input('~task name~: ')
        except NameError:
            user_input = input('~task name~: ')
        if user_input == 'q':
            break

        tm.start_task(user_input)

if __name__ == '__main__':
    main()
