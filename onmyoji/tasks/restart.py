# -*- coding: utf-8 -*-

from .gameplay import GameplayTask

class RestartTask(GameplayTask):
    """Restart the game and automatically download any patches if required."""

    def run(self):
        self.logger.info('restarting game process ...')
        if self.is_game_running():
            self.device.force_stop('com.netease.onmyoji')
        self.device.start_package('com.netease.onmyoji')

        # FIXME just assume it's a first-time run and no update for now
        self.logger.info('performing first-time setup ...')
        self.wait(42)
        self.input.refresh_orientation()
        if not self.match_pixel((0, 0), '010101', timeout=12, retry=3):
            self.beep()
            self.prompt_input('if sees login screen, type Y to continue: ', 'y')
        else:
            self.hotspot('skip_openning')
