# -*- coding: utf-8 -*-

import random
import time
from ..core.utils import colors_distance, random_within_circle
from . import TaskFailedError
from .base import BaseTask

class GameplayTask(BaseTask):
    """Provide some useful functions for gameplay automation."""

    def hotspot(self, name):
        self.tap(*self.config('hotspot.' + name))

    def tap(self, *args):
        if len(args) == 4:
            x = args[0] + random.randint(0, args[2])
            y = args[1] + random.randint(0, args[3])
            self.input.tap(x, y)
        elif len(args) == 3:
            x, y = random_within_circle(args[0], args[1], args[2])
            self.input.tap(x, y)
        elif len(args) == 2:
            self.input.tap(args[0], args[1])
        else:
            raise TaskFailedError('invalid tap position {0}'.format(args))

    def is_game_running(self):
        """Check if game is running (may not be in foreground)."""
        return self.device.is_process_running('com.netease.onmyoji')

    def checkpoint(self, name, **kwargs):
        checkpoint = self.config('checkpoint.' + name)
        return self.match_pixel((checkpoint[0], checkpoint[1]), checkpoint[2][1:], **kwargs)

    def match_pixel(self, position, source, distance=0, timeout=0, retry=0):
        """
        Check the pixel color at specified position matches the given color
        code (e.g. ff00ff)
        """
        time_last = time.time()
        time_elapsed = 0
        while True:
            target = self.device.get_screen_pixel(*position)
            if colors_distance(source, target) <= distance:
                break

            time_now = time.time()
            time_delta = time_now - time_last
            time_elapsed += time_delta

            if time_elapsed >= timeout:
                return False
            time_last = time_now

            time_sleep = retry * 2 - time_delta
            if time_sleep > 0:
                time.sleep(time_sleep)
        return True

    def wait(self, time_sleep=1):
        """Pause task for specified time in seconds."""
        self.logger.debug('sleeping for %.5f', time_sleep)
        time.sleep(time_sleep)

    def random_wait(self, min_time=0, max_time=1):
        """Pause task for a random time between specified range."""
        time_sleep = random.uniform(min_time, max_time)
        self.logger.debug('sleeping for %.5f', time_sleep)
        time.sleep(time_sleep)
