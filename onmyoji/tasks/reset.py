# -*- coding: utf-8 -*-

from . import BaseTask

class ResetTask(BaseTask):
    """Reset the game data."""

    def run(self):
        self.logger.info('force stopping game ...')
        self.device.force_stop(self.config('app.package_name'))

        data_path = self.config('app.data_path')
        backup_path = '/backup.'.join(data_path.rsplit('/', 1))

        self.logger.info('backing up game data to [%s] ...', backup_path)
        self.device.move(data_path, backup_path)

        self.logger.info('deleting game data/cache ...')
        self.device.clear_package(self.config('app.package_name'))

        self.logger.info('recovering game data ...')
        self.device.move(backup_path, data_path)

        self.logger.info('deleting temporary files ...')
        for tmp in self.config('app.temp_data'):
            self.logger.info('deleting [%s] ...', tmp)
            self.device.delete(tmp)

        self.logger.info('rebuilding configuration ...')
        self.device.push('data/clientconfig', self.config('app.config_path'))
