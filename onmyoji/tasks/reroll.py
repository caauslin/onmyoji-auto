# -*- coding: utf-8 -*-

from .gameplay import GameplayTask

class RerollTask(GameplayTask):

    def run(self):
        self.manager.start_task('reset')
        self.manager.start_task('restart')
        self.wait(3.2)
        self.manager.start_task('login_as_guest')
        self.manager.start_task('play_ch1_tutorial')
