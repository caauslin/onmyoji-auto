# -*- coding: utf-8 -*-

import sys

class BaseTask(object):
    """Base class for game tasks."""

    def __init__(self, manager):
        self.manager = manager

    @property
    def device(self):
        return self.manager.get_device()

    @property
    def logger(self):
        return self.manager.get_logger()

    @property
    def input(self):
        return self.manager.get_input()

    def config(self, key):
        return self.manager.get_config(key)

    @staticmethod
    def beep():
        """Play a short warning sound."""
        try:
            import winsound
            winsound.Beep(750, 750)
        except ImportError:
            sys.stdout.write('\a')
            sys.stdout.flush()

    @staticmethod
    def prompt_input(prompt, options=None, ignore_case=True):
        """Read and return the user input."""
        if hasattr(options, '__iter__'):
            if ignore_case:
                options = [str(opt).lower() for opt in options]
        elif options is not None:
            if ignore_case:
                options = (str(options).lower(),)
            else:
                options = (str(options),)

        sys.stdout.write(prompt)
        sys.stdout.flush()
        while True:
            try:
                user_input = raw_input() # python2
            except NameError:
                user_input = input()

            if options is None:
                return user_input
            if ignore_case:
                user_input = user_input.lower()

            if user_input in options:
                return user_input
            sys.stdout.write('invalid input, re-enter: ')
            sys.stdout.flush()

    def run(self):
        """Task body are implemented here."""
        raise NotImplementedError('task is not implemented')

    def close(self):
        """Get called when task is completed or terminated."""
        pass
